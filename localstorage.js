// check if localstorage value is empty and set default
if (localStorage.getItem("sidebar") === null) 
{
  	localStorage.setItem("sidebar", "closed");
}

var storedlog = (localStorage["storedlog"]);  

if (localStorage.getItem("storedlog") === null) 
{
	//$('.hidebutton').hide();
  	//storedlog = "It was null, now welcome!";
}

// call sidebar variable from localstorage, not sure if this needs to be done??
localStorage.getItem("sidebar");

// set initial sidebar visibility and button text based on storage variable
if (localStorage.getItem("sidebar") == "closed")
{
	$('#activitylist').hide();
	$('.hidebutton').html("show log");
	//console.log ("sidebar status is closed");
}
else if (localStorage.getItem("sidebar") == "opened")
{
	$('#activitylist').show();
	$('.hidebutton').html("hide log");
	//console.log ("sidebar status is opened");
}

// on click on toggle button  check current sidebar status and act on it
$(".hidebutton").click(function()
{
	// if localstorage value is closed
	if (localStorage.getItem("sidebar") == "closed" )
	{
		$('#activitylist').show();
		$(this).html("hide log");
		localStorage.setItem("sidebar", "opened");
		console.log ("open sesame");
	}
	// if localstorage value is opened
	else if (localStorage.getItem("sidebar") == "opened" )
	{
		$('#activitylist').hide();
		$(this).html("show log");
		localStorage.setItem("sidebar", "closed");
		console.log ("close it up");
	}
	
});

//save current sidebar log status to localstorage whenever a person is toggled
$('.toggle').click(function(){
	localStorage["storedlog"] = ($("#activitylist").html());
});

// check if current sidebar log is empty and set back stored sidebar log from localstorage
if (localStorage.getItem("storedlog") === null)
	// if nothing is stored, set empty state
	{
		console.log ("nothing was stored")
		//$('#activitylist').html("It was null, now welcome!");
		
	}
	// get stored sidebar log
	else
	{ 
		$("#activitylist").html(localStorage.getItem("storedlog"));
		console.log ("something stored")

	}