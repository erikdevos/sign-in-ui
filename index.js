// List filter options 
var options = {
	valueNames: ['name', 'team', 'job']
};

// List var not currently used
var userList = new List('wrapper', options);


// Toggle class on element when you click on an item
$(".toggle").click(function() {
	$(this).toggleClass("selected");
});

// List the amount of people that are selected
$('.toggle').click(function(){
	// calculate nr of people who are out
	var peoplenrout = ($("#peoplelist ul .selected").length);
	// calculate nr of people total
	var peoplenrtot = ($("#peoplelist ul li").length);
	// calculate nr of people who are in
	var peoplenrin = peoplenrtot - peoplenrout;

	// How many people are currently in/out
	$('#peoplecount').html("<p class='peoplenr'>" + peoplenrout + " people are currently out. " + peoplenrin +" in.</p>");

	
	// if everyone is out
	if (peoplenrout === peoplenrtot) { 
		$('#peoplecount').html("<p class='peoplenr'>Everyone is out. Don't forget to close up!</p><style>body{background:darkgray!important;}</style>");
	}
	else if (peoplenrin === peoplenrtot) { 
		$('#peoplecount').html("<p class='peoplenr'>Full house! Is the beer in the fridge?</p><style>body{background:#d5ded2!important;}</style>");
	}
	
	// list people that are in/out
	else {
	$('#peoplecount').html("<p class='peoplenr'>" + peoplenrout + " people are currently out. " + peoplenrin +" in.</p>");
	}
});

// Get and display who just left or came in
$('.toggle').click(function(){
	// set variable
	var element = $(this);
	// get name of person
	var personname = element.children('.name').text();

// get the day, date, time
var d = new Date(),
	days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
	day = days[d.getDay()],
	months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
	month = months[d.getMonth()],
	date = d.getDate(),
	suf = ['th','st','nd','rd'],
	v = date%100,
	date = date+(suf[(v-20)%10]||suf[v]||suf[0]),
	hours = d.getHours(),
	minutes = d.getMinutes(),
	noon = '';
	// Add '0' to minutes if less than 10
	if(minutes<10) {
	  minutes ='0'+minutes;
	};
	
	// Output the Timestamp
	var timestamp = +hours+':'+minutes+' '+noon;
	//var timestamp = day+' '+month+' '+date+', '+hours+':'+minutes+' '+noon;


	// output name that was just toggled

	// if people leave
	if(element.hasClass('selected')) 
	{
		// if geraldo leaves
		if(personname === "Geraldo Dos Santos")
		{
			$('#personname').prepend("<p class='timestamp'>" + timestamp + "</p>" );
			$('#personname').prepend("<p class='personmessage'>" + personname + " just left to get some pizza.</p>" );		
		}
		// if erik leaves
		else if(personname === "Erik de Vos")
		{
			$('#personname').prepend("<p class='timestamp'>" + timestamp + "</p>" );
			$('#personname').prepend("<p class='personmessage'>" + personname + " left. Finally.</p>" );		
		}
		// if anyone else leaves
		else
		{
			$('#personname').prepend("<p class='timestamp'>" + timestamp + "</p>" );
			$('#personname').prepend("<p class='personmessage'>" + personname + " left.</p>" );
		}
	}
	
	// if people come back
	else  
	{
		// if geraldo comes back
		if(personname === "Geraldo Dos Santos")
		{
			$('#personname').prepend("<p class='timestamp'>" + timestamp + "</p>" );
			$('#personname').prepend("<p class='personmessage'>" + personname + " finished his pizza. This is not fine.</p>" );		
		}
		// if erik comes back
		else if(personname === "Erik de Vos")
		{
			$('#personname').prepend("<p class='timestamp'>" + timestamp + "</p>" );
			$('#personname').prepend("<p class='personmessage'>" + personname + " is back. Not again.</p>" );		
		}

		// if anyone else comes back
		else
		{

			$('#personname').prepend("<p class='timestamp'>" + timestamp + "</p>" );
			$('#personname').prepend("<p class='personmessage'>" + personname + " came back</p>");
		}
	}

});