# README #

This is the UI part of the Sign-in system being developed as part of the "Friday Afternoon Project" initiative.

The webapp doesn't need anything other than the files that are included and can run offline. It's mostly HTML, CSS, JS and jQuery with some images.


It's not actually functional as intended yet (besides the UI part) since there is no API and hardware attached to sync and store the data to. 

The only storage it uses it localstorage in the browser. This is used the same way cookies are used: to store settings and to store content offline.

### How do I get set up? ###

Get the files.
Open de index.html


### Current known issues ###
* There is no empty state for the log, so the sidebar shows nothing on first visit when no-one has checked in our out yet.
* The log in the sidebar is stored in localstorage, but it saved all data until the browser refuses. There should be a limit and a first-in first-out system to limit the length of the log.

### Who do I talk to? ###

* Repo owner